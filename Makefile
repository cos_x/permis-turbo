.PHONY: all clean install

CC = cc
DESTDIR =

all: permis.c
	$(CC) permis.c -o permis

clean:
	rm -f permis

install: all
	mkdir -p $(DESTDIR)/usr/local/bin/
	cp permis $(DESTDIR)/usr/local/bin/permis
