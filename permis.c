#include <stdio.h>

int main(int argc, char** argv)
{
    double x = 0;
    unsigned i = 0;
    do
    {
        printf("%.13f\t\t%u\n", x, i);
        x = 0.9990000000000001 * x + 1;
        i++;
    }
    while(x < (double) 1000);
    printf("%.13f\t\t%u\n", x, i);
}
